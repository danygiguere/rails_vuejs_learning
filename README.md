#### This is a Rails and vue.js demo project



##### to install this app:
    - bundle install
    - create a database with the name vuejs
    - rails db:migrate
    - rails db:seed
    - npm install 
    - npm run watch (or npm run production) to compile the js and scss.
    - rails s
    - go to http://localhost:3000/
    
##### demo
    - try creating an article without entering anything in the fields, you'll see errro messages.
    - then add a title and text and hit create.
    - The error messages are coming from the models in the back end. The are displayed in the vue.js component by errors.js an success.js.    
    - then go to : http://localhost:3000/search and try to search for the article by the title or text. 
    