class Article < ApplicationRecord
  validates :title, presence: { message: "The title is required" }
  validates :text, presence: { message: "The text is required" }

  belongs_to :user

end
