Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :users
  resources :articles
  root 'pages#home'
  get 'search', to: 'pages#search'
  get 'search', to: 'pages#search'
  get '/articles/search/:search_text', to: 'articles#search'

end
